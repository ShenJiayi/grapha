name := "graphA_solution"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
	"org.apache.spark" %% "spark-core" % "1.5.1",
	"org.apache.spark" %% "spark-sql" % "1.5.1"
)
libraryDependencies += "com.google.guava" % "guava" % "12.0"




