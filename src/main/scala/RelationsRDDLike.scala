/**
 * Created by zhangchengfei on 16-3-10.
 */
package spark.grapha

import spark.grapha.util.{OpenHashMap, ImmutableBitSet, ImmutableVector, ImmutableLongOpenHashSet}

import scala.reflect.ClassTag
import org.apache.spark._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import scala.collection.mutable.ArrayBuffer


private[grapha] trait RelationsRDDLike[@specialized(Long ,Int, Double) V,
  P[X] <: RelationsRDDPartitionLike[X, P],
  Self[X] <: RelationsRDDLike[X, P, Self]] extends RDD[(Long, V)] {

  // value值类型的ClassTag生成器
  protected implicit def vTag: ClassTag[V]

  // Partition类型的ClassTag生成器
  protected implicit def pTag[V2]: ClassTag[P[V2]]

  protected def self: Self[V]

  def partitionsRDD: RDD[P[V]]

  def withPartitionsRDD[V2: ClassTag](partitionRDD: RDD[P[V2]]): Self[V2]

  // 分区器
  override val partitioner =partitionsRDD.partitioner

  // 分区函数
  override protected def getPartitions: Array[Partition] = partitionsRDD.partitions

  // 与位置相关
  override protected def getPreferredLocations(s: Partition): Seq[String] =
    partitionsRDD.preferredLocations(s)

  // 存储方式，不是action操作
  override def persist(newLevel: StorageLevel): this.type = {
    partitionsRDD.persist(newLevel)
    this
  }

  // unpersist为true,这就让Spark来计算哪些RDD需要持久化,这样有利于提高GC的表现。
  override def unpersist(blocking: Boolean = true): this.type = {
    partitionsRDD.unpersist(blocking)
    this
  }

  // 统计元素数目
  override def count(): Long = {
    partitionsRDD.map(_.size).reduce(_ + _)
  }

  // 这些继承自RDD的类都定义了compute函数。
  // 该函数会在action操作被调用时触发，在函数内部是通过迭代器进行对应的转换操作
  override def compute(part: Partition, context: TaskContext): Iterator[(Long, V)] = {
    firstParent[P[V]].iterator(part, context).next.iterator
  }

  def getOutNeighbors(ks: Array[Long]) = {
    val ksByPartition = ks.groupBy(k => self.partitioner.get.getPartition(k))
    val partitions = ksByPartition.keys.toSeq
    val results = self.context.runJob(self.partitionsRDD,
      (context: TaskContext, partIter: Iterator[P[V]]) => {
        if (partIter.hasNext) {
          val part = partIter.next()
          part.getOutNeighbours(ks)
        }
      }, partitions, allowLocal = true
    )
    results
  }



  // 收集消息
  def getMessages() = {
    self.partitionsRDD.mapPartitions(_.map(_.getMessages()).flatten, preservesPartitioning = true)
    //val newPartitionsRDD = self.partitionsRDD.mapPartitions(it=>it.map(_.getMess()).flatten, preservesPartitioning = true)//.flatMap(_.toIterator)
  }

  // 收集消息
  def getMessages2() = {
    self.partitionsRDD.mapPartitions(_.map(_.getMessages2()).flatten, preservesPartitioning = true)
    //val newPartitionsRDD = self.partitionsRDD.mapPartitions(it=>it.map(_.getMess()).flatten, preservesPartitioning = true)//.flatMap(_.toIterator)
  }

  def aggregateUsingIndex(messages: RDD[(Long, (Int, Int))]) = {
    this.zipPartitionsWithOther(messages)(new AggregateUsingIndexZipper)
  }

  protected def zipPartitionsWithOther(other: RDD[(Long, (Int, Int))])(f: OtherZipPartitionsFunction) = {
    val newPartitionsRDD = self.partitionsRDD.zipPartitions(other, true)(f)
    withPartitionsRDD(newPartitionsRDD)
    //newPartitionsRDD
  }

  protected type OtherZipPartitionsFunction = Function2[Iterator[P[V]], Iterator[(Long, (Int, Int))], Iterator[P[V]]]

  private class AggregateUsingIndexZipper extends OtherZipPartitionsFunction with Serializable {
    def apply(thisIter: Iterator[P[V]], otherIter: Iterator[(Long, (Int, Int))]): Iterator[P[V]] = {
      val thisPart = thisIter.next()
      Iterator(thisPart.aggregateUsingIndex(otherIter))
    }
  }


  // for weight
  def aggregateUsingIndexWeight(messages: RDD[(Long, Double)]) = {
    this.zipPartitionsWithOtherWeight(messages)(new AggregateUsingIndexZipperWeight)
  }

  protected def zipPartitionsWithOtherWeight(other: RDD[(Long, Double)])(f: OtherZipPartitionsFunctionWeight) = {
    val newPartitionsRDD = self.partitionsRDD.zipPartitions(other, true)(f)
    withPartitionsRDD(newPartitionsRDD)
    //newPartitionsRDD
  }

  protected type OtherZipPartitionsFunctionWeight = Function2[Iterator[P[V]], Iterator[(Long, Double)], Iterator[P[V]]]

  private class AggregateUsingIndexZipperWeight extends OtherZipPartitionsFunctionWeight with Serializable {
    def apply(thisIter: Iterator[P[V]], otherIter: Iterator[(Long, Double)]): Iterator[P[V]] = {
      val thisPart = thisIter.next()
      Iterator(thisPart.aggregateUsingIndexWeight(otherIter))
    }
  }

}







































