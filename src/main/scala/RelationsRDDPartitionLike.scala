/**
 * Created by zhangchengfei on 16-3-20.
 */
package spark.grapha
import scala.collection.immutable.LongMap
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

import org.apache.spark.Logging
import util.BitSet
import util.ImmutableBitSet
import util.ImmutableLongOpenHashSet
import util.ImmutableVector
import util.OpenHashSet
import util.OpenHashMap
import util.PrimitiveVector

private[grapha] trait RelationsRDDPartitionLike[@specialized(Long, Int, Double) V,
  Self[X] <: RelationsRDDPartitionLike[X, Self]] extends Serializable with Logging {

  implicit def vTag: ClassTag[V]
  def self: Self[V]

  def index: ImmutableLongOpenHashSet
  def values: ImmutableVector[V]
  def neighbors_num: ImmutableVector[Int]
  def weight: ImmutableVector[Double]
  def mask: ImmutableBitSet

  def withIndex(index: ImmutableLongOpenHashSet): Self[V]
  def withValues[V2: ClassTag](values: ImmutableVector[V2]): Self[V2]
  def withNeighbor_num(neighbor_num: ImmutableVector[Int]): Self[V]
  def withWeight(weight: ImmutableVector[Double]): Self[V]
  def withMask(mask: ImmutableBitSet): Self[V]

  val capacity: Int = index.capacity

  def size: Int = mask.capacity

  def iterator: Iterator[(Long, V)] =
    mask.iterator.map(ind => (index.getValue(ind), values(ind)))

  def isDefined(k: Long): Boolean = {
    val pos = index.getPos(k)
    pos >= 0 && mask.get(pos)
  }

  def apply(k: Long): V = values(index.getPos(k))

  def getOutNeighbours(ks: Array[Long]) = {
    var result = LongMap.empty[V]
    var i = 0
    while (i < ks.length) {
      val k = ks(i)
      if (self.isDefined(k)) {
        result = result.updated(k, self(k))
      }
      i += 1
    }
    result
  }


  def getMessages() = {
    val map = new OpenHashMap[Long, (Int, Int)]

    self.mask.iterator.foreach(i => {
      val relations = self.values(i).asInstanceOf[Array[Long]]

      val count = relations.size
      map(self.index.getValue(i)) = (1, count)
    })
    map.toList
    //List((1L, 0.1), (2L, 0.3)).toMap
    //LongMap((1L, 0.1), (2L, 0.3))
    //LongMap((1L, 0.1), (2L, 0.3))//.toIterator
  }


  def getMessages2() = {
    val map = new OpenHashMap[Long, Double]
    self.mask.iterator.foreach(i => {
      val relations = self.values(i).asInstanceOf[Array[Long]]
      val n = relations.size//self.neighbors_num(i)
      val w = self.weight(i) / n
      val key = self.index.getValue(i)

      relations.foreach { v =>
        val key_value = map.getOrElse(v, 0.0)
        map(v) = w + key_value
      }
    })
    map.toList//.toIterator
  }


  // 聚合操作
  // 这里不考虑没有入度的点，拿网页举例就是没有任何其他网页指向这个网页，是没有价值的网页
  // 这里假设左边入度边的集合在右边出度边中都包含
  // [Long, (Byte, Byte, Double)]
  //  srcID, HashId, NeighborNum, weight
  def aggregateUsingIndex(iter: Iterator[Product2[Long, (Int, Int)]]) = {
    val map = new OpenHashMap[Long,  Int]

    /*iter.foreach { pair =>
      val key_value = map.getOrElse(pair._1._1, ArrayBuffer())
      if (key_value.isEmpty) {
        map(pair._1._1) = ArrayBuffer(pair._1._2)
      } else {
        map(pair._1._1) += pair._1._2
      }
    }

    val mapArray = new OpenHashMap[Long, T](map.size)
    map.foreach(x => {
      mapArray(x._1) = x._2.toArray
    })*/

/*
    iter.foreach { product =>
      val key = product._1
      val neighbor_num = product._2._2
      // val hash_num = product._2._1
      val weight = product._2._3 * 1.0 / neighbor_num
      val pos = self.index.getPos(key)
      val neighbors = self.values(pos).asInstanceOf[Array[Long]]
      //println("key:" + key + "    neighbor_num:" + neighbor_num)

      for (n <- neighbors) {
        val value = map.getOrElse(n, 0.0)
        map(n) = value + weight
        //println("neighbor:" + n)
      }
    }
    map.toArray*/


    /*(1L, 0.toByte, 0.toByte)*/



    //val newMask = new BitSet(self.capacity)
    val newValue = new Array[Int](self.capacity)
    //val newElements = new PrimitiveVector[Product2[Long, Double]]

    iter.foreach { product =>
      val id = product._1
      val value = product._2
      val pos = self.index.getPos(id)
      //map(id) = value._2
      newValue(pos) = value._2
    }

    //newValue.foreach(println)

    val aggregated = this.withNeighbor_num(ImmutableVector.fromArray(newValue))//.withMask(newMask.toImmutableBitSet)
    //println("+++++" + aggregated.neighbors_num.size)
    aggregated
  }

  // for weight
  def aggregateUsingIndexWeight(iter: Iterator[Product2[Long, Double]]) = {
    val newValue = new Array[Double](self.capacity)
    iter.foreach { product =>
      val id = product._1
      val value = product._2
      val pos = self.index.getPos(id)
      newValue(pos) = value * 0.85 + 0.15
      //println("key:" + id + "  value:" + (value * 0.85 + 0.15))
    }
    val aggregated = this.withWeight(ImmutableVector.fromArray(newValue))//.withMask(newMask.toImmutableBitSet)
    aggregated
  }

}




















































