/**
 * Created by zhangchengfei on 16-3-21.
 */
package spark.grapha
import scala.reflect.ClassTag
import spark.grapha.util.{OpenHashMap, ImmutableVector, ImmutableBitSet, ImmutableLongOpenHashSet}
import scala.collection.mutable.ArrayBuffer

private[grapha] class RelationsRDDPartition[@specialized(Long, Int, Double) V](
  val index: ImmutableLongOpenHashSet,
  val values: ImmutableVector[V],
  val neighbors_num: ImmutableVector[Int],
  val weight: ImmutableVector[Double],
  val mask: ImmutableBitSet

  )(implicit val vTag: ClassTag[V])
    extends RelationsRDDPartitionLike[V, RelationsRDDPartition] {

  def self: RelationsRDDPartition[V] = this

  def withIndex(index: ImmutableLongOpenHashSet): RelationsRDDPartition[V] = {
    new RelationsRDDPartition(index, values, neighbors_num, weight, mask)
  }

  def withValues[V2: ClassTag](values: ImmutableVector[V2]): RelationsRDDPartition[V2] = {
    new RelationsRDDPartition(index, values, neighbors_num, weight, mask)
  }

  def withNeighbor_num(neighbors_num: ImmutableVector[Int]): RelationsRDDPartition[V] = {
    new RelationsRDDPartition(index, values, neighbors_num, weight, mask)
  }

  def withWeight(Weight: ImmutableVector[Double]): RelationsRDDPartition[V] = {
    new RelationsRDDPartition(index, values, neighbors_num, weight, mask)
  }

  def withMask(mask: ImmutableBitSet): RelationsRDDPartition[V] = {
    new RelationsRDDPartition(index, values, neighbors_num, weight, mask)
  }
}

private[grapha] object RelationsRDDPartition {
  type T = Array[Long]
  type TT = (Double, Array[Long])

  def apply[V: ClassTag](iter: Iterator[((Long, Long), V)], resetProb: Double): RelationsRDDPartition[T] = {
    val map = new OpenHashMap[Long, Array[Long]]
    var pre: Long = -1  // 记录前一个源点ID
    var tmp = ArrayBuffer[Long]()
    val map2 = new OpenHashMap[Long, Double]
    iter.foreach { pair =>
      val src = pair._1._1
      val dst = pair._1._2

      if (pre == -1) {
        pre = src
      }
      if (src != pre) {
        map(pre) = tmp.toArray
        map2(pre) = resetProb
        pre = src
        tmp = ArrayBuffer(dst)
      } else {
        tmp += dst
      }
    }
    map(pre) = tmp.toArray
    map2(pre) = resetProb

    new RelationsRDDPartition(
      ImmutableLongOpenHashSet.fromLongOpenHashSet(map.keySet),
      ImmutableVector.fromArray(map.values),
      ImmutableVector.fromArray(Array.empty[Int]),
      ImmutableVector.fromArray(map2.values),
      map.keySet.getBitSet.toImmutableBitSet
    )
  }
}