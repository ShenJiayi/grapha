/**
 * Created by zhangchengfei on 16-3-22.
 */
package spark.grapha
import org.apache.spark.Partitioner

import scala.collection.mutable

class PartitionStrategy {

}

/**
 * 1D分区方法
 * @param numParts
 */
class EdgePartition1D(numParts: Int) extends Partitioner {
  override def numPartitions: Int = numParts
  override def getPartition(key: Any): Int = {
    val mixingPrime: Long = 1125899906842597L
    key match {
      case k1: (Long, Long) => {
        (math.abs(k1._1 * mixingPrime) % numParts).toInt
      }
      case k2: Long => {
        (math.abs(k2 * mixingPrime) % numParts).toInt
      }
      case _ => 0
    }
  }
}

class AdaptivePartition(numParts: Int) extends Partitioner {
  override  def numPartitions: Int = numParts
  var record = new mutable.HashMap[Long, (Int, Int)]() // param1:key, param2:目前分区号, param3:目前分区中数目

  var num:Int = 0
  def smartHash(src: Long, part: Int): Int = {
    val mixingPrime: Long = 1125899906842597L
    val srcHash = (math.abs(src * mixingPrime) % numParts)
    val span = (0 to 500).toArray   // 最多500分区
    val par = span.map(x => (x + srcHash) % numParts)
    par(part).toInt
  }




  override def getPartition(key: Any): Int = {
    key match {
      case k1: (Long, Long) => {
        // record没有记录
        if (!record.contains(k1._1)) {
          val a = smartHash(k1._1, 0) // 分配第一个分区
          record(k1._1) = (a, 1)
        } else if (record(k1._1)._2 > 5000) { // 当前分区中超过200个元素, 滑动到下一个分区窗口
          val a = smartHash(k1._1, record(k1._1)._1 + 1)
          record(k1._1) = (a, 1)
        } else { // 继续往这个分区存
          record(k1._1) = ((record(k1._1)._1), record(k1._1)._2 + 1)
        }
        //println("(long, Long) Adaptive Patition")
        //println(k1._1 + " " + record(k1._1)._1 + " " + record(k1._1)._2)
        record(k1._1)._1
      }
      case k2: Long => {
        if (!record.contains(k2)) {
          val a = smartHash(k2, 0)
          record(k2) = (a, 1)
        } else if (record(k2)._2 > 0) {
        val a = smartHash(k2, record(k2)._1 + 1)
          record(k2) = (a, 1)
        } else { // 继续往这个分区存
          record(k2) = ((record(k2)._1), record(k2)._2 + 1)
        }
        //println(k2 + " " + record(k2)._1 + " " + record(k2)._2)
        record(k2)._1
      }
      case _ => {
        0
      }
    }
  }
}

