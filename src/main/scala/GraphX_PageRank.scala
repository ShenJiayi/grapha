/**
 * Created by zhangchengfei on 16-3-26.
 */
package spark.graphx
import org.apache.spark.graphx.{GraphLoader, Edge, EdgeDirection, Graph}
import org.apache.spark.{SparkContext, SparkConf}

object GraphX_PageRank {
  def main(args: Array[String]) {
    if (args.length != 1) {
      System.err.println("Usage: GraphX_PageRank <edge file path>")
      System.exit(-1)
    }

    val sparkConf = new SparkConf().setAppName("GraphX_PageRank")

    val sc = new SparkContext(sparkConf)

    val start = System.nanoTime()
    val graph = GraphLoader.edgeListFile(sc, args(0), numEdgePartitions = 30)
    val ranks = graph.staticPageRank(2).vertices.cache()
    //val testTmp = ranks.collect()
    //testTmp.foreach(println)
    val end = System.nanoTime()
    val t = (end - start) / 1000000.0
    println(s"总共耗时：${t}ms")

  }
}
