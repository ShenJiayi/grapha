/**
 * Created by zhangchengfei on 16-3-21.
 */
package spark.grapha
import scala.collection.mutable
import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import scala.reflect.{classTag, ClassTag}
import org.apache.spark._
import org.apache.spark.rdd.RDD

class RelationsRDD[@specialized(Long, Int, Double) V: ClassTag](
  val partitionsRDD: RDD[RelationsRDDPartition[V]])
  extends RDD[(Long, V)](partitionsRDD.context, List(new OneToOneDependency(partitionsRDD)))
  with RelationsRDDLike[V, RelationsRDDPartition, RelationsRDD] {

  override protected def vTag: ClassTag[V] = classTag[V]
  override protected def pTag[V2]: ClassTag[RelationsRDDPartition[V2]] =
    classTag[RelationsRDDPartition[V2]]

  override protected def self: RelationsRDD[V] = this

  def withPartitionsRDD[V2: ClassTag](
    partitionsRDD: RDD[RelationsRDDPartition[V2]]): RelationsRDD[V2] = {
    new RelationsRDD(partitionsRDD)
  }

/*
  override def partitionBy(partitionStrategy: PartitionStrategy) = {
    partitionBy()
  }
*/


}

object RelationsRDD {
  type T = Array[Long]
  type TT = (Double, Array[Long])

  def apply[V: ClassTag](elems: RDD[((Long, Long), V)], resetProb: Double = 0.15): RelationsRDD[T] = {
    RelationsRDD(elems, resetProb, elems.partitioner.getOrElse(new EdgePartition1D(elems.partitions.size)))
    //RelationsRDD(elems, resetProb, elems.partitioner.getOrElse(new AdaptivePartition(elems.partitions.size)))
  }

  def apply[V: ClassTag](elems: RDD[((Long, Long), V)], resetProb: Double, partitioner: Partitioner): RelationsRDD[T] = {
    val partitioned: RDD[((Long, Long), V)] = elems.partitionBy(partitioner)

    val partitions = partitioned.mapPartitions(
      iter => Iterator(RelationsRDDPartition(iter, resetProb)),
      preservesPartitioning = true) // preservesPartitioning是否保留父RDD的partitioner分区信息
    new RelationsRDD(partitions)
  }
}

