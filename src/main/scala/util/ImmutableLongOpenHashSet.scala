/**
 * Created by zhangchengfei on 16-3-20.
 */
package spark.grapha.util

import com.google.common.hash.Hashing

private[grapha] class ImmutableLongOpenHashSet(
  val data: ImmutableVector[Long],
  val bitset: ImmutableBitSet,
  val focus: Int, // focused的元素位置
  loadFactor: Double) extends Serializable {

  import OpenHashSet.{INVALID_POS, NONEXISTENCE_MASK, Hasher, LongHasher}

  private val hasher: Hasher[Long] = new LongHasher

  def size: Int = bitset.size()
  def capacity: Int = data.size

  private def mask = capacity - 1
  private def growThreshold = (loadFactor * capacity).toInt

  // 返回集合是否包含某个特定元素
  def contains(k: Long): Boolean = getPos(k) != INVALID_POS

  // 返回元素的位置，没有返回INVALID_POS
  def getPos(k: Long): Int = {
    var pos = hashcode(hasher.hash(k)) & mask
    var i = 1

    val maxProbe = capacity  // 探针最大长度不能超过数组大小

    while (i < maxProbe) {
      if (!bitset.get(pos)) { // 不存在
        return INVALID_POS
      } else if (k == data(pos)) { // 找到
        return pos
      } else {
        // val delta =
        pos = (pos + i) & mask
        i += 1
      }
    }
    // 不会执行下面
    INVALID_POS
  }



  // 二次hash
  // 这里用的是google的陌陌hash，比传统hash算法要快几十倍
  private def hashcode(h: Int): Int = Hashing.murmur3_32().hashInt(h).asInt()

  def add(k: Long): ImmutableLongOpenHashSet = {
    addWithoutResize(k).rehashIfNeeded(ImmutableLongOpenHashSet.grow, ImmutableLongOpenHashSet.move)
  }


  // 返回特定位置的值
  def getValue(pos: Int): Long = data(pos)

  // 将一个元素加入到集合中，但不会触发rehashing
  //
  def addWithoutResize(k: Long): ImmutableLongOpenHashSet =  {
    var pos = hashcode(hasher.hash(k)) & mask  // 获取hash位置

    var i = 1
    var result: ImmutableLongOpenHashSet = null
    while (result == null) {
      if (!bitset.get(pos)) { // 不存在，说明是一个新的key
        result = new ImmutableLongOpenHashSet(
        data.updated(pos, k), bitset.set(pos), pos | NONEXISTENCE_MASK, loadFactor
        )
        // 将数据存到Array ，将对应的bit位设置为1
      } else if (data(pos) != k) { // 该索引存在但存的值不是k
      val delta = i
        pos = (pos + delta) & mask // 这里采用线性探测法解决hash冲突
        i += 1
      } else {  // 该索引存在并且存的值是k
        result = new ImmutableLongOpenHashSet(data, bitset, pos, loadFactor)
      }
    }
    result
  }

  // 如果超过负载需要进行rehash
  // allocateFunc分配一个更大的数组
  // moveFunc 将数据从原来的数组move到新的数组
  def rehashIfNeeded(allocateFunc: (Int) => Unit, moveFunc: (Int, Int) => Unit): ImmutableLongOpenHashSet = {
    if (size > growThreshold) {
      rehash(allocateFunc, moveFunc)
    } else {
      this
    }
  }

  private def rehash(allocateFunc: (Int) => Unit, moveFunc: (Int, Int) => Unit): ImmutableLongOpenHashSet = {
    val newCapacity = capacity * 2  // 空间进行2被扩展

    allocateFunc(newCapacity) // 申请两倍大的空间

    val newBitset = new BitSet(newCapacity)
    val newData = new Array[Long](newCapacity)
    val newMask = newCapacity - 1

    var oldPos = 0

    // 将数据迁移到新的数组中
    while (oldPos < capacity) {
      if (bitset.get(oldPos)) { // 该位置有数据
      val key = data(oldPos)
        var newPos = hashcode(hasher.hash(key)) & newMask
        /** 下面是否需要优化，直接对数据进行拷贝 */
        var i = 1
        var keepGoing = true
        while (keepGoing) {
          if (!newBitset.exist(newPos)) { // 该位置为空
            newData(newPos) = key
            newBitset.set(newPos)
            moveFunc(oldPos, newPos)
            keepGoing = false
          } else { // 已经存了数据需要解决冲突
            //val delta = i
            newPos = (newPos + i) & newMask
            i += 1
          }
        }
      }
      oldPos += 1
    }

    new ImmutableLongOpenHashSet(
      ImmutableVector.fromArray(newData), newBitset.toImmutableBitSet, -1, loadFactor
    )
  }




}

private[grapha] object ImmutableLongOpenHashSet {
  def fromLongOpenHashSet(set: OpenHashSet[Long]): ImmutableLongOpenHashSet =
    new ImmutableLongOpenHashSet(
      ImmutableVector.fromArray(set.data), set.getBitSet.toImmutableBitSet, -1, set.loadFactor)


  def empty(initialCapacity: Int, loadFactor: Double): ImmutableLongOpenHashSet =
    new ImmutableLongOpenHashSet(
      ImmutableVector.fromArray(new Array[Long](initialCapacity)),
      new ImmutableBitSet(initialCapacity), -1, loadFactor)

  def empty(initialCapacity: Int): ImmutableLongOpenHashSet = empty(initialCapacity, 0.7)

  def empty(): ImmutableLongOpenHashSet = empty(64, 0.7)

  private def growl(newSize: Int) {}
  private def movel(oldPos: Int, newPos: Int) {}

  private val grow = growl _
  private val move = movel _
}
