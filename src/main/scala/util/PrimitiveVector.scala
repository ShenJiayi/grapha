/**
 * Created by zhangchengfei on 16-3-12.
 */
package spark.grapha.util
import scala.reflect.ClassTag

private[grapha]
class PrimitiveVector[@specialized(Long, Int, Double) V: ClassTag](initialSize: Int = 64) {
  private var _numElements = 0
  private var _array: Array[V] = _

  _array = new Array[V](initialSize)

  // 获取对应的key的value值
  def apply(index: Int): V = {
    require(index < _numElements)
    _array(index)
  }

  def +=(value: V): Unit = {
    if (_numElements == _array.length) {
      resize(_array.length * 2)
    }
    _array(_numElements) = value
    _numElements += 1
  }

  def capacity: Int = _array.length
  def length: Int = _numElements

  def size: Int = _numElements

  def array: Array[V] = _array

  // 让vector的capacity等于size
  def trim(): PrimitiveVector[V] = resize(size)

  // 重置数组大小，若总的长度减少，则丢弃元素
  def resize(newLength: Int): PrimitiveVector[V] = {
    val newArray = new Array[V](newLength)
    _array.copyToArray(newArray)
    _array = newArray
    if (newLength < _numElements) {
      _numElements = newLength
    }
    this
  }

}
