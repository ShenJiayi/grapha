/**
 * Created by zhangchengfei on 16-3-12.
 */
package spark.grapha.util
import scala.reflect._

private[spark]
class OpenHashMap[@specialized(Long, Int, Double) K: ClassTag,
@specialized(Long, Int, Double) V: ClassTag] (
initialCapacity: Int) extends Iterable[(K, V)] with Serializable {

  // 构造函数，默认容量为64
  def this() = this(64)

  // key的类型只能是Long或Int
  require(classTag[K] == classTag[Long] || classTag[K] == classTag[Int])

  protected var _keySet: OpenHashSet[K] = _
  private var _values: Array[V] = _
  _keySet = new OpenHashSet[K](initialCapacity)
  _values = new Array[V](_keySet.capacity)

  private var _oldValues: Array[V] = null

  //override def size = _keySet.size


  private[spark] def keySet = _keySet
  private[spark] def values = _values

  // 返回对应key的value
  def apply(k: K): V = {
    val pos = _keySet.getPos(k)  // 返回元素位置
    _values(pos)  // 返回值
  }

  // 设置一个key的value值
  def update(k: K, v: V): Unit = {
    val pos = _keySet.addWithoutResize(k) & OpenHashSet.POSITION_MASK
    _values(pos) = v
    _keySet.rehashIfNeeded(k, grow, move) // 是否要rehash
    _oldValues = null // 释放原来的空间
  }

  // 获取某个指定key的value，不存在则返回elseValue
  def getOrElse(k: K, elseValue: V): V = {
    val pos = _keySet.getPos(k)
    if (pos >= 0) _values(pos) else elseValue
  }



  override def iterator = new Iterator[(K, V)] {
    var pos = 0
    var nextPair: (K, V) = computeNextPair()

    // 获取下一个value值
    def computeNextPair(): (K, V) = {
      pos = _keySet.nextPos(pos)
      if (pos >= 0) {
        val ret = (_keySet.getValue(pos), _values(pos))
        pos += 1
        ret
      } else {
        null
      }
    }

    def hasNext = nextPair != null

    def next() = {
      val pair = nextPair
      nextPair = computeNextPair()
      pair
    }
  }



  protected var grow = (newCapacity: Int) => {
    _oldValues = _values
    _values = new Array[V](newCapacity)
  }


  protected var move = (oldPos: Int, newPos: Int) => {
    _values(newPos) = _oldValues(oldPos)
  }



}