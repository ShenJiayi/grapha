/**
 * Created by zhangchengfei on 16-3-20.
 */
package spark.grapha.util


private[grapha] class ImmutableBitSet(val numBits: Int, val words: ImmutableVector[Long])
extends Serializable {
  // 构造函数
  def this(numBits: Int) =
    this(numBits, ImmutableVector.fromArray(new Array(ImmutableBitSet.bit2words(numBits))))

  // long 数目
  private def numWords: Int = ImmutableBitSet.bit2words(numBits)

  def capacity: Int = numWords * 64 // 实际容量大小


  // 返回特等index的bit为的值，存在返回1，不存在返回0
  def get(index: Int): Boolean = {
    val bitmask = 1L << (index & 0x3f)  // 获取Long中存储的值，低6位
    (words(index >> 6) & bitmask) != 0
  }

  // 将特定的index的位置设置为true然后返回一个新的ImmutableBitSet
  def set(index: Int): ImmutableBitSet = {
    val wordIdx = index >> 6 // 获取word的index
    val bitmask = 1L << (index & 0x3f) // mask
    val oldWord = words(wordIdx)
    val newWord = oldWord | bitmask
    if (newWord == oldWord)
      this
    else
      new ImmutableBitSet(numBits, words.updated(wordIdx, newWord))

  }

  // 返回set中为true的位数
  def size(): Int = {
    var sum = 0
    val iter = words.iterator
    while (iter.hasNext) {
      sum += java.lang.Long.bitCount(iter.next())
    }
    sum
  }

  // 产生迭代器
  def iterator = words.iterator.zipWithIndex.flatMap(pair => wordIterator(pair._1, pair._2 * 64))

  private def wordIterator(word: Long, globalOffset: Int): Iterator[Int] = new Iterator[Int] {
    private[this] var w = word
    private[this] var o = 0
    override def hasNext: Boolean = w != 0 && o < 64

    override def next() = {
      val step = java.lang.Long.numberOfTrailingZeros(w)
      val result = o + step + globalOffset
      w = w >>> step + 1
      o += step + 1
      result
    }
  }

}


private[grapha] object ImmutableBitSet {
  // 返回对应bit位数需要多少个long来存储
  private def bit2words(numBits: Int) = ((numBits -1) >> 6) + 1
}