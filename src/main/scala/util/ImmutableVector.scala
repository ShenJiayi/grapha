/**
 * Created by zhangchengfei on 16-3-12.
 */
package spark.grapha.util
import scala.reflect.ClassTag

// 支持点的高效更新
// 与scala.collection.immutable.Vector相似，用32个元素的Array作为叶子的32叉树实现的
// 与scala的Vector不同的是，它对值的类型进行限制，对于primitive的值内存中更高效
private[grapha]
class ImmutableVector[@specialized(Long, Int) A](val size: Int, root: VectorNode[A])
  extends Serializable {
  // 迭代
  def iterator: Iterator[A] = new VectorIterator[A](root)
  // 获取
  def apply(index: Int): A = root(index)
  // 更新
  def updated(index: Int, elem: A): ImmutableVector[A] = new ImmutableVector(size, root.updated(index, elem))
}

private sealed trait VectorNode[@specialized(Long, Int) A] extends Serializable {
  def apply(index: Int): A
  def updated(index: Int, elem: A): VectorNode[A]
  def numChildren: Int
}

// 返回vector tree的iterator
private class VectorIterator[@specialized(Long, Int) A](v: VectorNode[A]) extends Iterator[A] {
  private[this] val elemStack: Array[VectorNode[A]] = Array.fill(8)(null)// [this]只有同一个对象中可见
  private[this] val idxStack: Array[Int] = Array.fill(8)(-1)  // elemStack和idxStack大小都是8
  private[this] var pos: Int = 0
  private[this] var _hasNext: Boolean = _
  private[this] var numLeafChildren = 0

  elemStack(0) = v
  idxStack(0) = 0
  maybeAdvance()


  override def hasNext = _hasNext

  override def next() = {
    if (_hasNext) {
      val result = elemStack(pos)(idxStack(pos))
      idxStack(pos) += 1
      if (idxStack(pos) >= numLeafChildren) maybeAdvance()
      result
    } else {
      throw new NoSuchElementException("end of iterator")
    }
  }

  private def maybeAdvance(): Unit = {
    while (pos >= 0 && idxStack(pos) >= elemStack(pos).numChildren) {
      pos -= 1
    }

    _hasNext = pos >= 0

    if (_hasNext) {
      var continue: Boolean = true
      while (continue) {
        elemStack(pos) match {
          case internal: InternalNode[_] =>
            val child = internal.childAt(idxStack(pos))
            idxStack(pos) += 1

            pos += 1
            elemStack(pos) = child
            idxStack(pos) = 0
          case leaf: LeafNode[_] =>
            numLeafChildren = leaf.numChildren
            continue = false
        }
      }
    }

  }
}

private[grapha] object ImmutableVector {
  def fromArray[A: ClassTag](array: Array[A]): ImmutableVector[A] = {
    fromArray(array, 0, array.length)
  }

  def fromArray[A: ClassTag](array: Array[A], start: Int, end: Int): ImmutableVector[A] = {
    new ImmutableVector(end - start, nodeFromArray(array, start, end))
  }

  // 返回32叉树根
  // 下面是递归调用
  private def nodeFromArray[A: ClassTag](array: Array[A], start: Int, end: Int): VectorNode[A] = {
    val length = end - start
    if (length == 0) {
      emptyNode
    } else {
      val depth = depthOf(length)  // lenght从1开始，depth从0开始
      if (depth == 0) {
        new LeafNode(array.slice(start, end))
      } else {
        val shift = 5 * depth // depth从0开始

        // length从1开始，计算子节点数目，实际就是从第1层开始依次计算子节点数目（depth从0开始）
        val numChildren = ((length - 1) >> shift) + 1
        val children = new Array[VectorNode[A]](numChildren) // new 创建子节点
        var i = 0

        // 开始构建第depth层的子节点
        while (i < numChildren) {
          val childStart = start + (i << shift) // 左下标
          var childEnd = start + ((i + 1) << shift) // 右下标
          if (end < childEnd) {
            childEnd  = end
          }

          children(i) = nodeFromArray(array, childStart, childEnd) // 递归构建
          i += 1
        }
        // 完成上述的构建之后，只返回之间节点的一个对象
        new InternalNode(children, depth)
      }
    }
  }


  private def emptyNode[A: ClassTag] = new LeafNode(Array.empty)

  // 计算n个元素树需要多少层
  private def depthOf(size: Int): Int = {
    var depth = 0
    var sizeLeft = (size - 1) >> 5 // 每个叶子可以放2^32个值， length从1开始所以要先减1
    while (sizeLeft > 0) {
      sizeLeft >>= 5  // 每层最多也只能有32个分支
      depth += 1
    }
    depth
  }
}

private class InternalNode[@specialized(Long, Int) A: ClassTag](children: Array[VectorNode[A]], val depth: Int)
  extends VectorNode[A] {

  require(children.length > 0, "InternalNode must have children")
  require(children.length <= 32, s"nodes cannot have more than 32 children (got ${children.length})")
  require(depth >= 1, s"InternalNode must have depth >= 1 (got $depth)")

  // 返回第i个节点的所有子节点
  def childAt(index: Int): VectorNode[A] = children(index)


  // 查询每个index的值
  override def apply(index: Int): A = {
    var cur: VectorNode[A] = this
    var continue: Boolean = true
    var result: A = null.asInstanceOf[A]

    while (continue) {
      cur match {
        case internal: InternalNode[A] =>
          val shift = 5 * internal.depth
          val localIndex = (index >> shift) & 31
          cur = internal.childAt(localIndex)
        case leaf: LeafNode[A] =>
          continue = false
          result = leaf(index & 31)
      }
    }
    result
  }

  // 对某个值进行更新
  override def updated(index: Int, elem: A) = {
    val shift = 5 * depth
    val localIndex = (index >> shift) & 31
    val childIndex = index & ~(31 << shift)

    val newChildren = new Array[VectorNode[A]](children.length)
    System.arraycopy(children, 0, newChildren, 0, children.length)
    newChildren(localIndex) = children(localIndex).updated(childIndex, elem)
    new InternalNode(newChildren, depth)
  }

  override def numChildren = children.length

}





// 向量树的左节点
private class LeafNode[@specialized(Long, Int) A: ClassTag] (children: Array[A]) extends VectorNode[A] {

  require(children.length <= 32, s"nodes cannot have more than 32 children (got ${children.length})")

  // 返回对应的index的值
  override def apply(index: Int): A = children(index)

  // 这里更新需要将整个子树都进行拷贝
  override def updated(index: Int, elem: A) = {
    val newChildren = new Array[A](children.length)
    System.arraycopy(children, 0, newChildren, 0, children.length) // 拷贝到新的数组中
    newChildren(index) = elem // 更新权值
    new LeafNode(newChildren)
  }

  override def numChildren = children.length

}
