package spark.grapha.util

/**
 * Created by zhangchengfei on 16-3-10.
 */


import scala.reflect._ // for ClassTag
import com.google.common.hash.Hashing

/**
 * OpenHashSet比Java标准的HashSet要更快，内存开销更小
 * 使用二次探测法解决冲突
 * 接两个参数，分别是hash表容量和负载因子（内部设置）
 */
private[util]
class OpenHashSet[@specialized(Long, Int) T: ClassTag](
  initialCapacity: Int, private[util] val loadFactor: Double)
  extends Serializable {

  require(initialCapacity <= (1 << 29), "Capacity不能超过2^29个元素")
  require(initialCapacity >= 1, "无效Capacity")
  require(loadFactor > 0.0, "负载因子必须大于0.0")
  require(loadFactor < 1.0, "负载因子必须小于1.0")

  import OpenHashSet._  // 引入object OpenHashSet中的类

  def this(initialCapacity: Int) = this(initialCapacity, 0.7)  // 0.7是负载因子
  def this() = this(64)  // 默认构造函数，与上面构造函数的顺序不能颠倒



  // 受保护的成员是从该成员定义的类的子类才能访问。
  protected val hasher: Hasher[T] = {
    implicitly[ClassTag[T]] match {  // implicitly隐式转换
      case ClassTag.Long => (new LongHasher).asInstanceOf[Hasher[T]]
      case ClassTag.Int => (new IntHasher).asInstanceOf[Hasher[T]]
      case _ => new Hasher[T]
    }
  }

  private def nextPowerOf2(n: Int): Int = {
    //highestOneBit取n这个数的二进制形式最左边的最高一位且高位后面全部补零
    val highBit = Integer.highestOneBit(n)
    // 已经达到最大值，返回最大值，否则乘以2倍返回
    if (highBit == n) n else highBit << 1
  }


  protected var _capacity = nextPowerOf2(initialCapacity)
  protected var _mask = _capacity - 1
  protected var _size = 0
  protected var _growThreshold = (loadFactor * _capacity).toInt // 下一次进行扩展的阈值


  // 设置bitset
  protected var _bitset = new BitSet(_capacity)  // 构建
  def getBitSet = _bitset

  // 利用scala编译器初始化Array构造，而不是声明
  protected var _data: Array[T] = _
  _data = new Array[T](_capacity)

  private[util] def data: Array[T] = _data

  // 集合中存入元素个数
  def size: Int = _size

  // 集合的容量
  def capacity: Int = _capacity

  // 如果包含指定的元素则放回true
  def contains(k: T): Boolean = getPos(k) != INVALID_POS

  // 添加一个元素到集合中
  // 若插入元素后集合大小超过容量，对集合进行扩容并重新hash元素
  def add(k: T) {
    addWithoutResize(k)  // 先将元素加入
    rehashIfNeeded(k, grow, move) // 再进行是否需要扩容，rehash判断。
  }

  // 返回一个已存储元素的下一个位置
  def nextPos(fromPos: Int): Int = _bitset.nextSetBit(fromPos)

  // 返回指定位置的值
  def getValue(pos: Int): T = _data(pos)

  // 将一个元素加入到集合中，但不会触发rehashing
  //
  def addWithoutResize(k: T): Int =  {
    var pos = hashcode(hasher.hash(k)) & _mask  // 获取hash位置

    var i = 1
    while (true) {
      if (!_bitset.exist(pos)) { // 不存在，说明是一个新的key
        _data(pos) = k // 将数据存到Array
        _bitset.set(pos) // 将对应的bit位设置为1
        _size += 1 // 数目加1
        return pos | NONEXISTENCE_MASK
      } else if (_data(pos) != k) { // 该索引存在但存的值不是k
        val delta = i
        pos = (pos + delta) & _mask // 这里采用线性探测法解决hash冲突
        i += 1
      } else {  // 该索引存在并且存的值是k
        return pos
      }
    }
    INVALID_POS
  }

  // 如果超过负载需要进行rehash
  // allocateFunc分配一个更大的数组
  // moveFunc 将数据从原来的数组move到新的数组
  def rehashIfNeeded(k: T, allocateFunc: (Int) => Unit, moveFunc: (Int, Int) => Unit): Unit = {
    if (_size > _growThreshold) {
      rehash(k, allocateFunc, moveFunc)
    }
  }

  private def rehash(k: T, allocateFunc: (Int) => Unit, moveFunc: (Int, Int) => Unit) {
    val newCapacity = _capacity * 2  // 空间进行2被扩展

    allocateFunc(newCapacity) // 申请两倍大的空间

    val newBitset = new BitSet(newCapacity)
    val newData = new Array[T](newCapacity)
    val newMask = newCapacity - 1

    var oldPos = 0

    // 将数据迁移到新的数组中
    while (oldPos < capacity) {
        if (_bitset.exist(oldPos)) { // 该位置有数据
          val key = _data(oldPos)
          var newPos = hashcode(hasher.hash(key)) & newMask
          /** 下面是否需要优化，直接对数据进行拷贝 */
          var i = 1
          var keepGoing = true
          while (keepGoing) {
            if (!newBitset.exist(newPos)) { // 该位置为空
              newData(newPos) = key
              newBitset.set(newPos)
              moveFunc(oldPos, newPos)
              keepGoing = false
            } else { // 已经存了数据需要解决冲突
              //val delta = i
              newPos = (newPos + i) & newMask
              i += 1
            }
          }
        }
      oldPos += 1
    }

    _bitset = newBitset
    _data = newData
    _capacity = newCapacity
    _mask = newMask
    _growThreshold = (loadFactor * newCapacity).toInt
  }






  // 二次hash
  // 这里用的是google的陌陌hash，比传统hash算法要快几十倍
  private def hashcode(h: Int): Int = Hashing.murmur3_32().hashInt(h).asInt()


  // 返回元素的位置，没有返回INVALID_POS
  def getPos(k: T): Int = {
    var pos = hashcode(hasher.hash(k)) & _mask
    var i = 1

    val maxProbe = _data.size  // 探针最大长度不能超过数组大小

    while (i < maxProbe) {
      if (!_bitset.exist(pos)) { // 不存在
        return INVALID_POS
      } else if (k == _data(pos)) { // 找到
        return pos
      } else {
        // val delta =
        pos = (pos + i) & _mask
        i += 1
      }
    }
    // 不会执行下面
    INVALID_POS
  }


}


private[grapha]
object OpenHashSet {
  val INVALID_POS = -1 // 无效位置
  val NONEXISTENCE_MASK = 0x80000000 // 不存在mask
  val POSITION_MASK = 0xEFFFFFF    // 总共28位， 1110 1111 1111 1111 1111 1111 1111


  // sealed 可以防止在文件外继承
  sealed class Hasher[@specialized(Long, Int) T] extends Serializable {
    def hash(o: T): Int = o.hashCode()
  }

  class LongHasher extends Hasher[Long] {
    override def hash(o: Long): Int = (o ^ (o >>> 32)).toInt // >>> 无符号右移补0，^是异或，同号为0异号为1
    // (o >>> 32)表示o的高32位，再与o进行异或，转成int（取低32位）
  }

  class IntHasher extends Hasher[Int] {
    override def hash(o: Int): Int = o
  }

  private def growl(newSize: Int) {}
  private def movel(oldPos: Int, newPos: Int) {}

  // 分配一个更大的数组
  private val grow = growl _  // 下划线和含义是将方法转成函数
  // 将数据迁移到新的数组中
  private val move = movel _
}
