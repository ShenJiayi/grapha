package spark.grapha.util

/**
 * Created by zhangchengfei on 16-3-11.
 * 固定大小的BIt Set
 * 没有安全/边界检查，所以速度比较快
 */
class BitSet(numBits: Int) extends Serializable {

  // long是64位，需要多少个long来存储，说白了这里就是用一个bit位表示一个数
  // 先减1再加1，比如100 0000只需要一个long，而100 0001需要两个long
  // 直接右移动6位不行，所以要先减1再加1
  private def bit2words(numBits: Int) = ((numBits -1) >> 6) + 1 // 就是右移6位

  private val words = new Array[Long](bit2words(numBits))
  private val numWords = words.length

  // 返回实际的bit位数
  def capacity: Int = numWords * 64

  // 某个索引位置是否存在，存在返回true，否则返回false
  def exist(index: Int): Boolean = {
    // index & 0x3f
    val bitmask = 1L << (index & 0x3f)
    (words(index >> 6) & bitmask) != 0
  }

  // 将bit位设置为1
  def set(index: Int): Unit = {
    val bitmask = 1L << (index & 0x3f)
    words(index >> 6) |= bitmask
  }

  private[spark] def toImmutableBitSet: ImmutableBitSet = {
    new ImmutableBitSet(numBits, ImmutableVector.fromArray(words))
  }

  // 从某个位置开始查找下一个位置，不存在返回-1
  def nextSetBit(fromIndex: Int): Int = {
    var wordIndex = fromIndex >> 6 // 返回数据存在第几个long中
    if (wordIndex >= numWords) {
      return -1
    }

    // 尝试查找当前word中的下一个set bit
    val subIndex = fromIndex & 0x3f  // 取出word的值
    var word = words(wordIndex) >> subIndex // 看高位是否还有值
    if (word != 0) { // 还有值
      // numberOfTrailingZeros  // 返回在指定 long 值的二进制补码表示形式中最低位（最右边）的 1 位之后的零位的数量。
      return (wordIndex << 6) + subIndex + java.lang.Long.numberOfTrailingZeros(word)
    }

    // 下一个bit在下一个word中
    wordIndex += 1
    while (wordIndex < numWords) {
      word = words(wordIndex)
      if (word != 0) {
        return (wordIndex << 6) + java.lang.Long.numberOfTrailingZeros(word)
      }
      wordIndex += 1
    }
    -1
  }

}
