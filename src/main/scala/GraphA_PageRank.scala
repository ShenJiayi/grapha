package spark.grapha

import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by zhangchengfei on 16-3-26.
 */
object GraphA_PageRank {
  def main(args: Array[String]) {
    if (args.length != 1) {
      System.err.println("Usage: GraphA_PageRank <edge file path>")
      System.exit(-1)
    }

    val sparkConf = new SparkConf().setAppName("GraphA_PageRank")

    val sc = new SparkContext(sparkConf)
    val start = System.nanoTime()

    val data = sc.textFile(args(0), 30).map { line =>
      val field = line.split(" ")
      ((field(0).toLong, field(1).toLong), 0L)
    }
    var relations = RelationsRDD(data, 0.15).cache()

    var i = 2
    while (i > 0) {
      val mess = relations.getMessages2().combineByKey(
        (v: Double) => v,
        (c: Double, v: Double) => c + v,
        (c1: Double, c2: Double) => c1 + c2,
        new EdgePartition1D(relations.partitions.size)
      )
      relations = relations.aggregateUsingIndexWeight(mess)
      i -= 1
      relations.foreachPartition(x => {})
    }

    val end = System.nanoTime()
    val t = (end - start) / 1000000.0
    println(s"总共耗时：${t}ms")
  }
}
